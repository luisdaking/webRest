/**
 * SensorController
 *
 * @description :: Server-side logic for managing sensors
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	new: function (req, res) {
    var content = req.body;

    if((content.name != undefined) && (content.value != undefined)){
      Sensor.create(content).exec(function (err, finn) {
        if(err){
          res.serverError();
        }
      });

      res.send({
        "state": "ok"
      });
    }else{
      res.send({
        "state": "error"
      })
    }
  },
  all: function (req, res) {

  },

  view: function (req, res) {

    var name = req.param('name');
    Sensor.find({
      where: {
        name: name
      }
    }).exec(function (error, records) {
        if(error){
          res.send([]);
        }
        res.send(records);
    })

  }
};

